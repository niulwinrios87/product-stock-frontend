export interface IProduct {
  _id: string;
  name: string;
  sku: string;
  description: string;
  image: string;
  price: number;
  stock: number;
}
