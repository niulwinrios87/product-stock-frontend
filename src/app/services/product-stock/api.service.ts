import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IPagination, IProduct } from 'src/app/interfaces';

@Injectable({
  providedIn: 'root',
})
export class ProductApiService {
  constructor(private http: HttpClient) {}

  getAllProducts(
    filters?: Record<string, string | number>
  ): Observable<IPagination<IProduct>> {
    let params: Record<string, string> = {};

    for (const key in filters) {
      if (filters?.[key]) {
        params = { ...params, [key]: String(filters[key]) };
      }
    }

    const filterParams = new URLSearchParams(params);

    return this.http.get<IPagination<IProduct>>(
      `http://localhost:4080/products?${filterParams}`
    );
  }

  getById(productId: string) {
    return this.http.get<IProduct>(`http://localhost:4080/products/${productId}`);
  }

  create(data: Omit<IProduct, 'id'>) {
    return this.http.post('http://localhost:4080/products', data);
  }

  update(data: Omit<IProduct, 'id'>) {
    const { _id, ...rest } = data;
    return this.http.patch(`http://localhost:4080/products/${_id}`, rest);
  }

  deleteProduct(productId: string) {
    return this.http.delete<IProduct>(`http://localhost:4080/products/${productId}`);
  }
}
