import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductStockRoutingModule } from './product-stock-routing.module';
import { ProductStockComponent } from './product-stock.component';
import { ProductRegisterComponent } from '../product-register/product-register.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FiltersComponent } from './filters/filters.component';


@NgModule({
  declarations: [
    ProductStockComponent,
    ProductRegisterComponent,
    FiltersComponent
  ],
  imports: [
    CommonModule,
    ProductStockRoutingModule,
    ReactiveFormsModule,
  ],
})
export class ProductStockModule { }
