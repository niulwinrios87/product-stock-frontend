import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.css']
})
export class FiltersComponent {
  constructor(
    private formBuilder: FormBuilder
  ) {}

  filters = this.formBuilder.group({
    name: [''],
    sku: [''],
  });

  @Output() onFilter = new EventEmitter();

  handleFilter() {
    this.onFilter.emit(this.filters.value);
  }
}
