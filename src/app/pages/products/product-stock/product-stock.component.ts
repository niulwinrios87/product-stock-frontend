import { Component, OnInit } from '@angular/core';
import { ProductApiService } from '../../../services/product-stock/api.service';
import { IProduct } from '../../../interfaces/IProduct';
import { IPaginationMetadata } from 'src/app/interfaces/IPagination';
import { Subject } from 'rxjs';

type filters = {
  name?: string;
  sku?: string;
};

@Component({
  selector: 'app-product-stock',
  templateUrl: './product-stock.component.html',
  styleUrls: ['./product-stock.component.css'],
})
export class ProductStockComponent implements OnInit {
  products: IProduct[] = [];
  filters: IPaginationMetadata & filters = {
    page: 0,
    per_page: 5,
    count: 10,
  };
  showModalProduct = false;
  productId: Subject<string> = new Subject<string>();

  constructor(private productApiService: ProductApiService) {}

  ngOnInit() {
    this.getAllProducts();
  }

  getAllProducts(
    params?: Partial<Pick<IProduct, 'name' | 'sku'>> & IPaginationMetadata
  ): void {
    this.productApiService
      .getAllProducts({ ...this.filters, ...params })
      .subscribe((response) => {
        this.products = response.data;
        this.filters = response.meta_data;
      });
  }

  deleteProduct(productId: string) {
    this.productApiService.deleteProduct(productId).subscribe(() => {
      this.getAllProducts(this.filters)
    });
  }

  handleProductPopup(active: boolean) {
    this.showModalProduct = active;
  }

  totalPages(): number[] {
    return new Array(Math.ceil(this.filters?.count / this.filters?.per_page));
  }

  handleEdit(productId: string) {
    this.productId.next(productId);
    this.handleProductPopup(true);
  }

  handlePage(page: number) {
    this.getAllProducts({ ...this.filters, page });
  }
}
