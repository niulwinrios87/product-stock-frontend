import {
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { IProduct } from 'src/app/interfaces';
import { ProductApiService } from 'src/app/services/product-stock/api.service';

@Component({
  selector: 'app-product-register',
  templateUrl: './product-register.component.html',
  styleUrls: ['./product-register.component.css'],
})
export class ProductRegisterComponent {
  formData = this.formBuilder.group({
    name: ['', Validators.required],
    description: ['', Validators.required],
    sku: ['', Validators.required],
    price: ['', Validators.required],
    stock: ['', Validators.required],
    image: [''],
    _id: [''],
  });

  constructor(
    private formBuilder: FormBuilder,
    private productApiService: ProductApiService
  ) {}

  @Output() onCloseModal = new EventEmitter();
  @Output() getAllProducts = new EventEmitter();
  @Input('showModal') showModal = false;
  @Input('productId') productId: Subject<string> = new Subject();

  ngOnInit() {
    this.productId.subscribe((productId) => {
      if (productId) {
        this.productApiService.getById(productId).subscribe((product) => {
          this.formData.setValue({
            description: product.description,
            image: product.image,
            name: product.name,
            price: String(product.price),
            sku: product.sku,
            stock: String(product.stock),
            _id: product._id,
          });
        });
      }
    });
  }

  onSubmit() {
    this.formData.value._id ? this.updateProduct() : this.createProduct();
  }

  createProduct() {
    this.productApiService
      .create(this.formData.value as unknown as Omit<IProduct, 'id'>)
      .subscribe(() => {
        this.getAllProducts.emit();
        this.handleCloseModal();
      });
  }

  updateProduct() {
    this.productApiService
      .update({
        ...this.formData.value,
      } as unknown as IProduct)
      .subscribe(() => {
        this.getAllProducts.emit();
        this.handleCloseModal();
      });
  }

  handleCloseModal() {
    this.onCloseModal.emit();
  }
}
